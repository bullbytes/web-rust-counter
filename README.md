# What is this?
A simple counter web app written in Rust, using [Yew](https://github.com/yewstack/yew).

# Getting started
* Install [rustup](https://rustup.rs/). If you're on Arch Linux, do `pacman -Sy cargo`
* `cargo install cargo-web`
* `rustup default nightly`
* Start the web app with `cargo web start` and visit `http://localhost:8000` in your browser
