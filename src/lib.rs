extern crate stdweb;
extern crate yew;

use yew::prelude::*;
use yew::services::ConsoleService;

pub struct Model {
  count: i8,
  console: ConsoleService,
}

pub enum Msg {Increment, Decrement}

impl Component for Model {
  type Message = Msg;
  type Properties = ();

  fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
    Model {
        count: 5,
        console: ConsoleService::new(),
    }
  }

  fn update(&mut self, _msg: Self::Message) -> ShouldRender {
      match _msg{
          Msg::Increment => {
              self.count = self.count + 1;
              self.console.log("one more arrow");
          }
          Msg::Decrement => {
              self.count = self.count - 1;
              self.console.log("one arrow less");
          }
      }
      // We rerender the view at every change of the model, unconditionally
    true
  }
}

impl Renderable<Model> for Model {
  fn view(&self) -> Html<Self> {
    html! {
            <div>
            <h1>{"Let's count!"}</h1>
            <div>
              <span>{&format!("Counter: {}", self.count)}</span>
              <div><button onclick=|event| Msg::Increment>{ "Increment counter" }</button></div>
              <div><button onclick=|event| Msg::Decrement>{ "Decrement counter" }</button></div>
            </div>
            </div>
    }
  }
}
